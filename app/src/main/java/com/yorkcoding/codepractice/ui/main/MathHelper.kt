package com.yorkcoding.codepractice.ui.main

class MathHelper {
    companion object {
        @Throws(Exception::class)
        fun mean(doubleArray: DoubleArray): Double {
            // if count of doubleArray is 0, then throw exception since median can not be calculated
            if (doubleArray.count() == 0) {
                throw Exception("Double array cannot be empty.")
            }

            return doubleArray.average()
        }

        @Throws(Exception::class)
        fun median(doubleArray: DoubleArray): Double {

            // if count of doubleArray is 0, then throw exception since median can not be calculated
            if (doubleArray.count() == 0) {
                throw Exception("Double array cannot be empty.")
            }

            // sorting the array first
            doubleArray.sort()

            // if count of double array is even, get the average of the middleIndex's value and the (middleIndex-1)'s value
            // if count of double array is odd, get the value of the middleIndex's value
            val middleIndex = doubleArray.count() / 2
            val median: Double
            if (doubleArray.count() % 2 == 0) {
                median = (doubleArray[middleIndex] + doubleArray[middleIndex - 1]) / 2
            }
            else {
                median = doubleArray[middleIndex]
            }
            return median
        }
    }
}